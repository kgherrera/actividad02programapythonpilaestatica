'''
Created on 3/10/2021

@author: Herrera
'''

class Pelicula:
    def __init__(self, nombrePelicula, generoPelicula):
        self.nombrePelicula = nombrePelicula
        self.generoPelicula = generoPelicula

class ImplementacionPilaEstatica:
    def __init__(self, listaPeliculas, cima, tam):
        self.cima = cima
        self.listaPeliculas = listaPeliculas
        self.tam = tam
    
    
    def pilaVacia(self):
        return self.cima == -1
    
    def pilaLlena(self):
        if(len(self.listaPeliculas) >= self.tam): 
            return True
        else: 
            return False
    
    def devolverPelicula(self, pelicula):
        if(not(ImplementacionPilaEstatica.pilaLlena(self))):
            self.cima+=1
            self.listaPeliculas.append(pelicula)
            return True
        else:
            return False
            
    def rentarPelicula(self):
        if(not(ImplementacionPilaEstatica.pilaVacia(self))):
            self.listaPeliculas.pop()
            self.cima-=1
            return True
        else:
            return False
        
    def mostrarCantidadPeliculas(self):
        return self.cima+1
    
listaPeliculas = []
cima = -1
tam = int(input("Introduce tama\xf1o de la pila: "))
pe1 = ImplementacionPilaEstatica(listaPeliculas, cima, tam)

p1 = Pelicula("Bob esponja", "Dibujo animado");
p2 = Pelicula("Volver al futuro", "Ficcion");
p3 = Pelicula("Caroline", "Suspenso");
p4 = Pelicula("Kung fu panda", "Animacion 3d");
p5 = Pelicula("choky", "Terror");

opcion = 0



while(opcion != 5):
    print("\nElige una de las siguientes opciones");
    print("1) Cargar BD de peliculas");
    print("2) Rentar pelicula");
    print("3) Devolver pelicula");
    print("4) Mostrar cantidad de peliculas disponibles");
    print("5) Salir");
    opcion = int(input("Introduce Opcion: "))
    
    if(opcion == 1):
        if(pe1.devolverPelicula(p1)): print("Se agrego la pelicula") 
        else: print("Pila llena")
        if(pe1.devolverPelicula(p2)): print("Se agrego la pelicula") 
        else: print("Pila llena")
        if(pe1.devolverPelicula(p3)): print("Se agrego la pelicula") 
        else: print("Pila llena")
        if(pe1.devolverPelicula(p4)): print("Se agrego la pelicula") 
        else: print("Pila llena")
        if(pe1.devolverPelicula(p5)): print("Se agrego la pelicula") 
        else: print("Pila llena")
        
    elif(opcion == 2):
        if(pe1.rentarPelicula()) : print("\nPelicula rentada correctamente")
        else: print("\nNa hay peliculas")
    elif(opcion == 3):
        print("\nIntroduce datos de la pelicula")
        nombre = input("Nombre: ")
        genero = input("Genero: ")
        p6 = Pelicula(nombre, genero)
        if(pe1.devolverPelicula(p6)): print("\nPelicula registrada correctamente")
        else: print("\nPila Llena")
    elif(opcion == 4):
        print(f"cantidad de peliculas disponibles: {pe1.mostrarCantidadPeliculas()}")
    elif(opcion == 5):
        print("\nSaliendo . . .")
    else:
        print("Opcion Incorrecta")
        
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        